# BMS_CWS

DAML code - prototype of the blockchain-based management platform for co-working spaces.

Reference: Fiorentino, Stefania and Bartolucci, Silvia, New Governance Perspectives on the Sharing Economy. 
A Blockchain Application for the ‘Smart’ Management of Co-Working Spaces with a Return for Local Authorities. 
Available at SSRN: https://ssrn.com/abstract=3478870 (2019).

Requirement:
Installation of DAML SDK: open source from https://daml.com 
Java
Visual studio

